const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const cors = require('cors')({origin: true});
const validator = require('validator');

exports.register = functions.https.onRequest((req, res) => {
	if (req.method === 'PUT') {
		res.status(403).send('Forbidden!');
		return;
	}
	cors(req, res, () => {
		let name = req.query.name;
		//validations
		if (!name) {
			res.status(200).send("Please enter name.");
			return;
		}
		let email = req.query.email;
		if (!email) {
			res.status(200).send("Please enter email.");
			return;
		}
		let password = req.query.password;
		if (!password) {
			res.status(200).send("Please enter password.");
			return;
		}
		if (!validator.isLength(password, 7)) {
			res.status(200).send("Please enter valid password.");
			return;
		}
		if(!validator.isEmail(email)){
			res.status(200).send("Please enter valid email.");
			return;
		}

		//check if user already exists in firestore
		var userRef = admin.firestore().collection('users').add({
				name: name,
				email: email,
				password: password
			}).then(ref => {
				console.log('add user account', ref.id);
				res.status(200).send("User account created.");
				return;
			});

	});
});
