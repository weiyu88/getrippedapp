package com.romaric.getripped;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

public class WorkoutAdapter extends RecyclerView.Adapter<WorkoutAdapter.ViewHolder> {

//    ArrayList<String> workoutTitle;
//    ArrayList<Integer> workoutImages;
//    ArrayList<Integer> workoutImageDetail;
//    ArrayList<String> workoutDescription;
    List<Workout> workoutList;
    Context context;
    private FirebaseAnalytics mFirebaseAnalytics;

//    public WorkoutAdapter(ArrayList<String> workoutTitle,ArrayList<Integer> workoutImages,ArrayList<Integer> workoutImageDetail,ArrayList<String> workoutDescription, Context context) {
//        this.workoutTitle = workoutTitle;
//        this.workoutImages = workoutImages;
//        this.workoutImageDetail = workoutImageDetail;
//        this.workoutDescription = workoutDescription;
//        this.context = context;
//    }

    public WorkoutAdapter(List<Workout> workoutList, Context context) {
        this.workoutList = workoutList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.workout_item,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final String title = workoutList.get(position).getWorkoutTitle();
        final int image = workoutList.get(position).getWorkoutImages();
        final int imageDetail = workoutList.get(position).getWorkoutImageDetail();
        final String description = workoutList.get(position).getWorkoutDescription();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        holder.workoutTextView.setText(title);
        Glide.with(context).load(image).into(holder.workoutImageView);

        holder.workoutRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,DetailsActivity.class);
                intent.putExtra("title",title);
                intent.putExtra("image",image);
                intent.putExtra("imageDetail", imageDetail);
                intent.putExtra("description",description);
                context.startActivity(intent);
            }
        });

        Bundle bundle = new Bundle();
        bundle.putString("workout_name",title);
        mFirebaseAnalytics.logEvent(title,bundle);
    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView workoutImageView;
        TextView workoutTextView;
        RelativeLayout workoutRelativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            workoutImageView = itemView.findViewById(R.id.workout_image);
            workoutTextView = itemView.findViewById(R.id.workout_text_view);
            workoutRelativeLayout = itemView.findViewById(R.id.relative_layout_workout);
        }
    }
}
