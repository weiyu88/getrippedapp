package com.romaric.getripped;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    TextView copyrightTextView, versionTextView, infoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        copyrightTextView = findViewById(R.id.copyright_text_view);
        versionTextView = findViewById(R.id.version_text_view);
        infoTextView = findViewById(R.id.info_dev_text_view);

        copyrightTextView.setText(R.string.copyright);
        versionTextView.setText(R.string.version);
        infoTextView.setText(R.string.info);
    }
}
