package com.romaric.getripped;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RegisterActivity extends AppCompatActivity {

    EditText registerNameEdittext, registerEmailEditText, registerPasswordEditText;
    TextView textView;
    Button registerButton;
    CheckBox checkBox;
    LinearLayout linearLayout;
    private FirebaseAuth mAuth;
    private static final String TAG = "EmailPassword";
    private FirebaseAnalytics mFirebaseAnalytics;

    private String FIREBASE_CLOUD_FUNCTION_REG_URL = "https://us-central1-getripped-bacbc.cloudfunctions.net/register ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerButton = findViewById(R.id.register_button);
        registerNameEdittext = findViewById(R.id.register_name_edit_text);
        registerEmailEditText = findViewById(R.id.register_email_edit_text);
        registerPasswordEditText = findViewById(R.id.register_password_edit_text);
        textView = findViewById(R.id.tnc_text_view);
        checkBox = findViewById(R.id.check_box);
        linearLayout = findViewById(R.id.linear_layout);

        mAuth = FirebaseAuth.getInstance();
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //prevent keyboard display when activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Bundle bundle = new Bundle();
        String name = "register_activity_screen";
        bundle.putString("register_screen",name);
        mFirebaseAnalytics.logEvent("register_activity",bundle);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //boolean mboolean;
                createUserWithEmailAndPassword(registerEmailEditText.getText().toString(),registerPasswordEditText.getText().toString());
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://getripped-bacbc.firebaseapp.com/term.html"));
                startActivity(intent);
            }
        });

        String tncMessage = getResources().getString(R.string.tnc_text_view);
        SpannableString text = new SpannableString(tncMessage);
        text.setSpan(new UnderlineSpan(),0,text.length(),0);
        textView.setText(text);

        linearLayout.setFocusableInTouchMode(true);
        linearLayout.requestFocus();

    }
    private void createUserWithEmailAndPassword (String email, String password){
        if (!validate()){
            return;
        }
        if (checkBox.isChecked()){
            checkBox.setError(null);
        }else {
            checkBox.setError("");
            Toast.makeText(this,"You must agree to the terms and conditions before registering!",Toast.LENGTH_SHORT).show();
            return;
        }

        //updateUiSuccess();
        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Authentication");
        progressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            //signupDialog();
                            //FirebaseUser user = mAuth.getCurrentUser();
                            registration();

                            //track sign up event
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.METHOD,"sing_up");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP,bundle);

                            Intent intent = new Intent(RegisterActivity.this,DashboardActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                            //updateUiFailed();
                        }

                        progressDialog.dismiss();
                    }
                });


    }
    private boolean validate(){
        boolean valid = true;
        String name = registerNameEdittext.getText().toString();
        if (name.isEmpty()||name.length()<3){
            registerNameEdittext.setError("at least 3 characters");
            valid = false;
        }else {
            registerNameEdittext.setError(null);
        }
        String email = registerEmailEditText.getText().toString();
        if (TextUtils.isEmpty(email)|| !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            registerEmailEditText.setError("enter a valid email address");
            valid = false;
        } else {
            registerEmailEditText.setError(null);
        }

        String password = registerPasswordEditText.getText().toString();
        if (TextUtils.isEmpty(password)||password.length()<8){
            registerPasswordEditText.setError("enter at least 8 character");
            valid = false;
        }else {
            registerPasswordEditText.setError(null);
        }
        return valid;
    }

//    public void onSignupSuccess(){
//        registerButton.setEnabled(true);
//        setResult(RESULT_OK,null);
//    }
//    public void onSignupFailed(){
//        Toast.makeText(getBaseContext(),"Register failed",Toast.LENGTH_LONG).show();
//        registerButton.setEnabled(true);
//    }
//
//    public void signup(){
//        if (!validate()){
//            onSignupFailed();
//            return;
//        }
//        registerButton.setEnabled(false);
//
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Creating Account...");
//        progressDialog.show();
//
//        new android.os.Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                onSignupSuccess();
//                progressDialog.dismiss();
//            }
//        },2000);
//    }
//    public void signupDialog(){
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Creating Account...");
//        progressDialog.show();
//
//        new android.os.Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                progressDialog.dismiss();
//            }
//        },2000);
//    }

    private void updateUiSuccess(){
        registerButton.setEnabled(false);
        registerEmailEditText.setEnabled(false);
        registerPasswordEditText.setEnabled(false);
        registerNameEdittext.setEnabled(false);
    }

    private void updateUiFailed(){
        registerButton.setEnabled(true);
        registerEmailEditText.setEnabled(true);
        registerPasswordEditText.setEnabled(true);
        registerNameEdittext.setEnabled(true);
        registerPasswordEditText.setText(null);
    }
    private void registration() {
        String name = registerNameEdittext.getText().toString();
        String email = registerEmailEditText.getText().toString();
        String password = registerPasswordEditText.getText().toString();

        HttpUrl.Builder httpBuilder = prepareRegRequestBuilder(name, email, password);
        sendMessageToCloudFunction(httpBuilder);
    }
    private HttpUrl.Builder prepareRegRequestBuilder(String name, String email, String password) {
        HttpUrl.Builder httpBuilder =
                HttpUrl.parse(FIREBASE_CLOUD_FUNCTION_REG_URL).newBuilder();
        httpBuilder.addQueryParameter("name", name);
        httpBuilder.addQueryParameter("email", email);
        httpBuilder.addQueryParameter("password", password);
        return httpBuilder;
    }
    void sendMessageToCloudFunction(HttpUrl.Builder httpBuilder) {

        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder().
                url(httpBuilder.build()).build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "error response firebase cloud function");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(RegisterActivity.this,
                                "Action failed please try gain.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) {
                ResponseBody responseBody = response.body();
                String resp = "";
                if (!response.isSuccessful()) {
                    Log.e(TAG, "action failed");
//                    resp = "Failed perform the action, please try again";
                } else {
                    try {
                        resp = responseBody.string();
                        Log.e(TAG, "Response " + resp);
                    } catch (IOException e) {
//                        resp = "Problem in reading response";
                        Log.e(TAG, "Problem in reading response " + e);
                    }
                }
//                runOnUiThread(responseRunnable(resp));
            }
        });
    }
}
