package com.romaric.getripped;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    RecyclerView recyclerView;
//    private ArrayList<String> imagesName = new ArrayList<>();
//    private ArrayList<Integer> images = new ArrayList<Integer>();
    List<Workout> workoutList = new ArrayList<>();
    BottomNavigationView bottomNavigationView;
    FrameLayout frameLayout;

    private FirebaseAuth mAuth;
    GoogleSignInClient googleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mAuth = FirebaseAuth.getInstance();

        bottomNavigationView = findViewById(R.id.bottom_nav_view);
        frameLayout = findViewById(R.id.main_frame);

        setFragment(new HomeFragment());
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        CustomAdapter adapter = new CustomAdapter(workoutList,this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        initializeData();
    }

    private void initializeData(){
        String [] name = getResources().getStringArray(R.array.workout_name);
        TypedArray imageId = getResources().obtainTypedArray(R.array.workout_image);

        for (int i=0; i<name.length;i++){
            workoutList.add(new Workout(name[i],imageId.getResourceId(i,0)));
//            workoutList.add(imageId.getResourceId(i,0));
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()){
                    case R.id.home_nav:
                        fragment = new HomeFragment();
                        setFragment(fragment);
                        return true;
                    case R.id.fav_nav:
                        fragment = new FavoriteFragment();
                        setFragment(fragment);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout_option:
                //mAuth.signOut();
//
////                googleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
////                            @Override
////                            public void onComplete(@NonNull Task<Void> task) {
////                                // ...
////                                Intent intent = new Intent(DashboardActivity.this,LoginActivity.class);
////                                startActivity(intent);
////                                finish();
////                            }
////                        });
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                // ...
                                Intent intent = new Intent(DashboardActivity.this,LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                break;
            case R.id.crash_option:
                Button crashButton = new Button(this);
                crashButton.setText("Crash!");
                crashButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Crashlytics.getInstance().crash(); // Force a crash
                    }
                });
                addContentView(crashButton,
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                break;
            case R.id.about_us:
                Intent intent_about = new Intent(this,AboutActivity.class);
                startActivity(intent_about);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame,fragment);
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
