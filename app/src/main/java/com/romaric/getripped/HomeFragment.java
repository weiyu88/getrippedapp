package com.romaric.getripped;


import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    List<Workout> workoutList = new ArrayList<>();

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
//        recyclerView = view.findViewById(R.id.recycler_view);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        CustomAdapter adapter = new CustomAdapter(workoutList,getActivity());
//        recyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//        initialize();
        return view;
    }
    public void initialize(){
        String [] name = getResources().getStringArray(R.array.workout_name);
        TypedArray imageId = getResources().obtainTypedArray(R.array.workout_image);

        for (int i=0; i<name.length; i++){
            workoutList.add(new Workout(name[i],imageId.getResourceId(i,0)));
        }
    }

}
