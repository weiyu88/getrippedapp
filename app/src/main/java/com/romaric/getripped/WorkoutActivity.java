package com.romaric.getripped;

import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class WorkoutActivity extends AppCompatActivity {

    RecyclerView workoutRecyclerView;
    List<Workout> workoutList = new ArrayList<>();
//    private ArrayList<String> workoutTitle = new ArrayList<>();
//    ArrayList<Integer> workoutImages = new ArrayList<>();
//    ArrayList<Integer> workoutImageDetail = new ArrayList<>();
//    ArrayList<String> workoutDescription = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        workoutRecyclerView = findViewById(R.id.recycler_view_workout);
        workoutRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        WorkoutAdapter adapter = new WorkoutAdapter(workoutList,this);
        workoutRecyclerView.setAdapter(adapter);

        String [] title = getResources().getStringArray(R.array.workout_a);
        String [] description = getResources().getStringArray(R.array.workout_a_description);
        TypedArray imageId = getResources().obtainTypedArray(R.array.workout_a_image);
        TypedArray imageDetailId = getResources().obtainTypedArray(R.array.workout_a_image_details);

        for (int i=0; i<title.length; i++){
           workoutList.add(new Workout(title[i],description[i],imageId.getResourceId(i,0),imageDetailId.getResourceId(i,0)));
//            workoutImages.add(imageId.getResourceId(i,0));
//            workoutImageDetail.add(imageDetailId.getResourceId(i,0));
//            workoutDescription.add(description[i]);
        }

        adapter.notifyDataSetChanged();

    }

//    private void initializeData(){
//        String [] title = getResources().getStringArray(R.array.workout_a);
//
//        for (int i=0; i<title.length; i++){
//            workoutTitle.add(title[i]);
//        }
//    }
}
