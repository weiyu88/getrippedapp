package com.romaric.getripped;

public class Workout {
    private String imageName;
    private String workoutTitle;
    private String workoutDescription;
    private int images;
    private int workoutImages;
    private int workoutImageDetail;

    public Workout(String imageName, int images) {
        this.imageName = imageName;
        this.images = images;
    }

    public String getImageName() {
        return imageName;
    }

    public int getImages() {
        return images;
    }

    public Workout(String workoutTitle, String workoutDescription, int workoutImages, int workoutImageDetail) {
        this.workoutTitle = workoutTitle;
        this.workoutDescription = workoutDescription;
        this.workoutImages = workoutImages;
        this.workoutImageDetail = workoutImageDetail;
    }

    public String getWorkoutTitle() {
        return workoutTitle;
    }

    public String getWorkoutDescription() {
        return workoutDescription;
    }

    public int getWorkoutImages() {
        return workoutImages;
    }

    public int getWorkoutImageDetail() {
        return workoutImageDetail;
    }
    //    public Workout(String imageName, String workoutTitle, String workoutDescription, int images, int workoutImages, int workoutImageDetail) {
//        this.imageName = imageName;
//        this.workoutTitle = workoutTitle;
//        this.workoutDescription = workoutDescription;
//        this.images = images;
//        this.workoutImages = workoutImages;
//        this.workoutImageDetail = workoutImageDetail;
//    }

//    public String getImageName() {
//        return imageName;
//    }
//
//    public String getWorkoutTitle() {
//        return workoutTitle;
//    }
//
//    public String getWorkoutDescription() {
//        return workoutDescription;
//    }
//
//    public int getImages() {
//        return images;
//    }
//
//    public int getWorkoutImages() {
//        return workoutImages;
//    }
//
//    public int getWorkoutImageDetail() {
//        return workoutImageDetail;
//    }

}
