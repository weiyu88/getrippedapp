package com.romaric.getripped;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

//    ArrayList<String> imageName = new ArrayList<>();
//    ArrayList<Integer> images = new ArrayList<>();
    List<Workout> workoutList;
    Context context;
    private FirebaseAnalytics mFirebaseAnalytics;

//    public CustomAdapter(Context context, ArrayList<String> imageName, ArrayList<Integer> images) {
//        this.imageName = imageName;
//        this.images = images;
//        this.context = context;
//    }


    public CustomAdapter(List<Workout> workoutList, Context context) {
        this.workoutList = workoutList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Glide.with(context).load(workoutList.get(position).getImages()).into(holder.dashboardImageView);
        holder.dashboardTextView.setText(workoutList.get(position).getImageName());

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = "WorkoutActivity";
                switch(position) {
                    case 0:
                        // Workout A
                        //track workout A event
                        Bundle bundle = new Bundle();
                        bundle.putString("click_chest_activity",name);
                        mFirebaseAnalytics.logEvent("Chest",bundle);

                        Intent intent = new Intent(context,WorkoutActivity.class);
                        context.startActivity(intent);
                        break;
                    case 1:
                        // Workout B
                        //track workout B event
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("click_back_activity",name);
                        mFirebaseAnalytics.logEvent("Back",bundle1);

                        Intent intent1 = new Intent(context,SecondWorkoutActivity.class);
                        context.startActivity(intent1);
                        break;
                    case 2:
                        //track workout c activity
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("click_leg_activity",name);
                        mFirebaseAnalytics.logEvent("Leg",bundle2);

                        Intent intent2 = new Intent(context,ThirdWorkoutActivity.class);
                        context.startActivity(intent2);
                        break;
                    case 3:
                        //track workout d activity
                        Bundle bundle3 = new Bundle();
                        bundle3.putString("click_bicep_activity",name);
                        mFirebaseAnalytics.logEvent("Biceps",bundle3);

                        Intent intent3 = new Intent(context,FourthWorkoutActivity.class);
                        context.startActivity(intent3);
                        break;
                    case 4:
                        //track workout e activity
                        Bundle bundle4 = new Bundle();
                        bundle4.putString("click_triceps_activity",name);
                        mFirebaseAnalytics.logEvent("Triceps",bundle4);

                        Intent intent4 = new Intent(context,FifthWorkoutActivity.class);
                        context.startActivity(intent4);
                        break;
                }
//                if (workout_name != null) {
//                    Intent intent = new Intent(context,WorkoutActivity.class);
//                    intent.putExtra("workout", workout_name);
//                    context.startActivity(intent);
//                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView dashboardImageView;
        TextView dashboardTextView;
        RelativeLayout relativeLayout;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            dashboardImageView = itemView.findViewById(R.id.dashboard_image_view);
            dashboardTextView = itemView.findViewById(R.id.dashboard_text_view);
            relativeLayout = itemView.findViewById(R.id.relative_layout);
            cardView = itemView.findViewById(R.id.card_view);
        }
    }
}
