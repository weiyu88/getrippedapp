package com.romaric.getripped;

import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class SecondWorkoutActivity extends AppCompatActivity {

    RecyclerView workoutRecyclerView;
    List<Workout> workoutList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        workoutRecyclerView = findViewById(R.id.recycler_view_workout);
        workoutRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        WorkoutAdapter adapter = new WorkoutAdapter(workoutList,this);
        workoutRecyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        initializeData();
    }

    private void initializeData(){
        String [] title = getResources().getStringArray(R.array.workout_b);
        String [] description = getResources().getStringArray(R.array.workout_b_description);
        TypedArray imageId = getResources().obtainTypedArray(R.array.workout_b_image);
        TypedArray imageDetailId = getResources().obtainTypedArray(R.array.workout_b_image_details);

        for (int i=0; i<title.length; i++){
            workoutList.add(new Workout(title[i],description[i],imageId.getResourceId(i,0),imageDetailId.getResourceId(i,0)));
//            workoutTitle.add(title[i]);
//            workoutImages.add(imageId.getResourceId(i,0));
//            workoutImageDetail.add(imageDetailId.getResourceId(i,0));
//            workoutDescription.add(description[i]);
        }
    }
}
