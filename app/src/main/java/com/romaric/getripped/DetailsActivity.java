package com.romaric.getripped;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

public class DetailsActivity extends AppCompatActivity {

    ImageView detailImageView, detailmageView2;
    TextView detailTitleTextView, detailDescriptionTextView;
    Button button;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        detailImageView = findViewById(R.id.detail_image_view);
        detailTitleTextView = findViewById(R.id.detail_title_text_view);
        detailDescriptionTextView = findViewById(R.id.detail_description_text_view);
        detailmageView2 = findViewById(R.id.detail_image_view2);
        button = findViewById(R.id.spotify);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        int image = intent.getIntExtra("image",0);
        int imageDetail = intent.getIntExtra("imageDetail",0);
        String description = intent.getStringExtra("description");
        //Spanned html = Html.fromHtml(description);

        detailTitleTextView.setText(title);
        Glide.with(this).load(image).into(detailImageView);
        Glide.with(this).load(imageDetail).into(detailmageView2);
        detailDescriptionTextView.setText(Html.fromHtml(description,null, new MyTagHandler()));

        //Action bar
        getSupportActionBar().setTitle(title);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = "com.spotify.music"; // getPackageName() from Context or Activity object

                Bundle bundle = new Bundle();
                String name = "SpotifyButton";
                bundle.putString("spotify_button",name);
                mFirebaseAnalytics.logEvent("spotify",bundle);

                if (isPackageInstalled(appPackageName,getPackageManager())){

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("spotify:artist:6VOK8y6IeYRgdss2xlvzc3"));
                    startActivity(intent);
                }
                else {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            }
        });
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
